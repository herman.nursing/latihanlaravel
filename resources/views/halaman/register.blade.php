@extends('layout.master')

@section('judul')
<p>Halaman form</p>

@endsection

@section('content')
    <h2>Buat Account Baru</h1>
    <h3>Sign up Form</h2>
    <form action="/welcome" method="post"> 
        @csrf
      <label> First Name :</label> <br><br>
      <input type="text" name="firstname"> <br> <br>
      <label for=""> Last Name:</label> <br> <br>
      <input type="text" name="lastname"> <br> <br>
      <label for="">Gender</label> <br> <br>
      <input type="radio" name="male"> Male <br>
      <input type="radio" name="female"> Female <br> <br>
      <label for="">Nationality</label> <br> <br>
      <select name="Warga Negara">
          <option value="1">Indonesia</option>
          <option value="2">Amerika</option>
          <option value="3">Ingris</option>
      </select> <br><br>
      <label for="">language Spoken</label> <br> <br>
      <input type="checkbox" name="ina"> Bahasa Indonesia <br>
      <input type="checkbox" name="en"> English <br>
      <input type="checkbox" name="other"> Other Language <br><br>
      <label for="bio">Bio</label> <br><br>
      <textarea name="biodata" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="sign up"></input>
    </form>
@endsection
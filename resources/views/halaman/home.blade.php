@extends('layout.master')

@section('judul')
<p>Halaman Judul</p>

@endsection

@section('content')

    <h2>Media Online</h1>
    <h3>Sosial Media Developer</h2>
    <p>Belajar dan berbagi agar hidup lebih baik</p>
    <h4>Benefit Join di Media Sosial</h3>
    <ul>
        <li>Mendapat motivasi dari sesama para Developer</li>
        <li>Sharing Knowledge</li>
        <li>Dibuat oleh calon Developer Web terbaik</li>
    </ul>
    <h4>Cara Bergabung ke Media Online</h3>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>

@endsection